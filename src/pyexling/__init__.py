from . import morph, parser_morph, parser_semantics, parser_syntax, syntax
from .as_class import PyExLing
from .processor import Processor
