from types import SimpleNamespace
from typing import Any, Dict
from xml.etree import ElementTree

from . import parser_morph, parser_semantics, parser_syntax
from .processor import Processor


class PyExLing:
    def __init__(self, host: str, service: str):
        self.processor = Processor(host, service)
        self.converter = self.LinguisticsConverter()

    def process(self, txt: str) -> Dict[str, Any]:
        return self.processor.process(text=txt)

    def lemmatize(self, word: str):
        processor_response_lexic = self.processor.process(
            word, only_morph=True
        )["lexics"]
        morph = parser_morph.create_annot_morph(processor_response_lexic)
        res = morph[0][0].get_lexeme().get_lemma()
        return res

    def txt2xml(self, text: str) -> ElementTree.Element:
        processed_text: Dict[str, Any] = self.process(text)
        xml = self.converter.convert(processed_text)
        return xml

    class LinguisticsConverter:
        def __init__(self):
            self.data: Dict[str, Any] = {}
            self.morph = None
            self.roles = None
            self.syntax = None

        def convert(self, data: Dict[str, Any]) -> ElementTree.Element:
            self.data = data
            self.morph = parser_morph.create_annot_morph(data["lexics"])
            self.roles = parser_semantics.create_annot_srl(data["roles"])
            self.syntax = parser_syntax.create_annot_syntax(data["syntax"])
            return self.make_element_for_doc()

        def make_element_for_doc(self) -> ElementTree.Element:
            el_document = ElementTree.Element("document")
            # TODO: remake loop to iter on sentences over syntax
            for syntax_idx in range(len(self.syntax)):
                el_document.append(self.make_element_for_sentence(syntax_idx))
            return el_document

        # TODO: rewrite to use sentence instead of sentence_idx
        def make_element_for_sentence(self, sentence_idx: int):
            syntax_sentence = self.syntax[sentence_idx]
            clauses = syntax_sentence.clauses

            el_sentence = ElementTree.Element(
                "sentence", idx=str(sentence_idx)
            )
            el_words = [
                self.make_element_for_word(sentence_idx, i)
                for i in range(len(syntax_sentence.words))
            ]

            for clause_idx, clause in enumerate(clauses):
                el_clause = ElementTree.SubElement(
                    el_sentence, "clause", idx=str(clause_idx)
                )
                for word_idx in clause:
                    el_clause.append(el_words[word_idx])

            if sentence_idx in self.roles.keys():
                roles = self.roles[sentence_idx]
                for role in roles:
                    e_role = ElementTree.SubElement(
                        el_sentence, "role", word_idx=str(role.word_idx)
                    )
                    for arg in role.arguments:
                        e_arg = ElementTree.SubElement(
                            e_role,
                            "argument",
                            word_idx=str(arg.word_idx),
                            role_id=str(arg.role_id),
                            role_name=arg.role_name(),
                        )

            return el_sentence

        def make_element_for_word(self, sentence_idx: int, word_idx: int):
            morph_word = self.morph[sentence_idx][word_idx]
            syntax_word = self.syntax[sentence_idx].words[word_idx]

            syntax_data = {
                "syntax_link_name": str(
                    syntax_word.get_link_name()
                    # .encode("cp1251")
                    # .decode("utf-8")
                ),
                "syntax_parent_idx": str(syntax_word.get_parent()),
            }

            lexeme_data = {}
            if len(morph_word.lexemes) > 0:
                nodes = dict(morph_word.lexemes[0].node)
                for key, item in nodes.items():
                    lexeme_data[key] = str(item)

            word_form = morph_word.get_form()
            word_len = len(word_form)

            begin_offset = morph_word.offset
            offsets = {
                "begin_offset": str(begin_offset),
                "end_offset": str(begin_offset + word_len),
            }

            el_word = ElementTree.Element(
                "word",
                form=word_form,
                idx=str(word_idx),
                **syntax_data,
                **lexeme_data,
                **offsets,
            )
            return el_word
