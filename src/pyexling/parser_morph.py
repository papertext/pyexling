from .morph import *


def create_annot_morph_sent(node):
    result = list()
    for word in node:
        result.append(Word(word))

    return result


def create_annot_morph(node):
    result = list()
    for sent in node:
        result.append(create_annot_morph_sent(sent))

    return result
