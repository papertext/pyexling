from .semantics import SemanticRelation, SRLArgument, SRLPredicate


def create_argument(node):
    arg = SRLArgument()
    arg.word_idx = node["wordNum"]
    arg.role_id = node["ucID"]
    return arg


def append_to_index(index, key, elem):
    if key in index:
        index[key].append(elem)
    else:
        index[key] = [elem]


def create_annot_srl(node):
    pred_index = {}
    for elem in node:
        idx = (elem["cplxSentNum"], elem["predNum"])

        append_to_index(
            pred_index, idx, (elem["predId"], create_argument(elem))
        )

    sent_index = {}
    for key, elem in list(pred_index.items()):
        idx = key[0]
        pred = SRLPredicate()
        pred.word_idx = key[1]
        pred.pred_id = elem[0][0]
        pred.arguments = [e[1] for e in elem]

        append_to_index(sent_index, idx, pred)

    return sent_index


def create_annot_semrel(node):
    sent_index = {}
    for elem in node:
        rel = SemanticRelation()
        rel.word_idx_left = elem["leftWordNum"]
        rel.word_idx_right = elem["rightWordNum"]
        rel.rel_id = elem["ucID"]
        append_to_index(sent_index, elem["leftSentNum"], rel)

    return sent_index
