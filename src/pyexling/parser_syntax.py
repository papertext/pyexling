# -*- coding: utf-8 -*-
from .syntax import *

MAX_WORD = 18446744073709551615


def sent_index_by_index_in_clause(sent, clause_index, index_in_clause):
    return sent.get_clauses()[clause_index][index_in_clause]


def create_annot_sentence(node):
    result = Sentence()

    result.words = [Word() for _ in range(len(node["base"]))]

    for i, elem in enumerate(node["clauses"]):
        new_clause = Clause()
        for item in elem["words"]:
            new_clause.append(item)
            result.words[item].clause_num = i

        new_clause.predicate = elem["predicate"]
        new_clause.subject = elem["subject"]

        result.clauses.append(new_clause)

    for word_index, elem in enumerate(node["base"]):
        word = result.words[word_index]
        word.link_name = elem["sLinkName"]
        word.parent = -1

        if elem["parentIndex"] != MAX_WORD:
            word.parent = sent_index_by_index_in_clause(
                result, word.clause_num, elem["parentIndex"]
            )
        elif elem["eParentIndex"] != MAX_WORD:
            word.parent = sent_index_by_index_in_clause(
                result, word.clause_num, elem["eParentIndex"]
            )

    for i, elem in enumerate(result.words):
        parent = elem
        for j, wd in enumerate(result.words):
            if wd.parent == i:
                parent.children.append(j)

    return result


def create_annot_syntax(node):
    result = list()
    for elem in node:
        result.append(create_annot_sentence(elem))

    return result
