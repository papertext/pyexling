# -*- coding: utf-8 -*-

import json
import sys
import urllib.error
import urllib.parse
import urllib.request

if sys.version_info >= (3, 0):
    import http.client as httplib
else:
    import http.client


class Processor(object):
    MALT_PARSER = 0
    AOT_PARSER = 1

    def __init__(self, service_addr, service_path):
        self.service_addr = str(service_addr)
        self.service_path = str(service_path)

    def process(self, text, only_morph=False, syntax_parser="malt"):
        conn = httplib.HTTPConnection(self.service_addr)

        synt_parser = 0
        if syntax_parser == "malt":
            synt_parser = 0
        elif syntax_parser == "aot":
            synt_parser = 1

        headers = {"Content-type": "text/json"}
        params = urllib.parse.urlencode({"format": "j", "PROTOCOL": "JSONRPC"})
        sendData = {
            "jsonrpc": "2.0",
            "method": "processText",
            "params": {
                "archive_format": "j",
                "output_format": "s",
                "text": text,
                "semsyn": False,
                "syntax_parser": synt_parser,
                "fix_prep": False,
                "only_morph": only_morph,
            },
            "id": "1601908510",
        }

        conn.request(
            "POST",
            self.service_path + "?" + str(params),
            json.dumps(sendData, ensure_ascii=False).encode("utf8"),
            headers,
        )

        #        response = conn.getresponse(buffering = True)
        response = conn.getresponse()
        data = response.read()
        if response.status != 200:
            raise Exception("Failed to retreive result from remote pipeline.")

        conn.close()

        ret = None

        try:
            ret = json.loads(data.decode("cp1251"))[
                "result"
            ]["value0"]
        except KeyError as e:
            print("-" * 30)
            print(
                json.loads(data.decode("cp1251"))["result"]
            )
            print("-" * 30)

        return ret
