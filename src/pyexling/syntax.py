class ClauseRelation:
    def __init__(self):
        self.left_clause = -1
        self.left_word = -1
        self.right_clause = -1
        self.right_word = -1
        self.link_name = str()

    def get_left_clause(self):
        return self.left_clause

    def get_right_clause(self):
        return self.right_clause

    def get_right_word(self):
        return self.right_word

    def get_left_word(self):
        return self.left_word

    def get_link_name(self):
        return self.link_name


class Sentence:
    def __init__(self):
        self.clauses = list()
        self.words = list()
        self.clause_relations = list()

    def get_clauses(self):
        return self.clauses

    def get_words(self):
        return self.words

    def get_clause_relation(self):
        return self.clause_relations

    def num_in_sent(self, cluase_num, num_in_clause):
        return self.clauses[cluase_num][num_in_clause]

    def num_in_clause(self, num_in_sent):
        for i, clause in enumerate(self.clauses):
            for j, item in enumerate(clause):
                if item == num_in_sent:
                    return i, j

        return None


class Clause(list):
    def __init__(self):
        self.subject = -1
        self.predicate = -1

    def get_subject(self):
        return self.subject

    def get_predicate(self):
        return self.predicate


class Word:
    def __init__(self):
        self.link_name = str()
        self.parent = -1
        self.children = list()
        self.clause_num = -1

    def get_link_name(self):
        return self.link_name

    def get_parent(self):
        return self.parent

    def get_children(self):
        return self.children

    def get_clause_num(self):
        return self.clause_num
