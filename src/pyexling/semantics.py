from .dict_rel_id import dict_rel_id
from .dict_role_id import dict_role_id


class SRLArgument:
    def __init__(self):
        self.word_idx = -1
        self.role_id = -1

    def __repr__(self):
        return f"word: {self.word_idx} role_id: {self.role_id}({dict_role_id[self.role_id]})"

    def role_name(self):
        return dict_role_id[self.role_id]


#    def __repr__(self):
#        return self.__unicode__().encode("utf8")


class SRLPredicate:
    def __init__(self):
        self.word_idx = -1
        self.pred_id = -1
        self.arguments = list()

    def __repr__(self):
        return "word: {} id: {} args: {}".format(
            str(self.word_idx),
            str(self.pred_id),
            str([str(e) for e in self.arguments]),
        )


#    def __repr__(self):
#        return self.__unicode__().encode("utf8")


class SemanticRelation:
    def __init__(self):
        self.word_idx_left = -1
        self.word_idx_right = -1
        self.rel_id = -1

    def __repr__(self):
        return "word left: {} word right: {} rel_id: {}({})".format(
            self.word_idx_left,
            self.word_idx_right,
            self.rel_id,
            dict_rel_id[self.rel_id],
        )


#    def __repr__(self):
#        return self.__unicode__().encode("utf8")
