def find_word_arg(srl_sent, word):
    for pred in srl_sent:
        for arg in pred.arguments:
            if arg.word_idx == word:
                return arg, pred

    return None, None
