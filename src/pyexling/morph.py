# -*- coding: utf-8 -*-


MORPH_UNDEFINED = 0


def make_str_from_l(lst, annex):
    return f"{annex}{lst}" if lst else ""


def make_str_from_v(val, annex):
    return f"{annex}{val}" if val.num != MORPH_UNDEFINED else ""


def get_vals(gram, mask):
    cases = gram & mask

    result = list()

    i = 1
    j = 0
    while j < 32:
        val = cases & i
        if val != 0:
            result.append(i)
        j += 1
        i = i << 1

    return result


def get_cases(gram):
    return get_vals(gram, 0x000000FF)


def get_genders(gram):
    return get_vals(gram, 0x00000700)


def get_numbers(gram):
    return get_vals(gram, 0x00001800)


def get_tense(gram):
    # return get_vals(gram, 0x0000E000)
    return gram & 0x0000E000


def get_persons(gram):
    return get_vals(gram, 0x00030000)


def get_lexeme_type(ucType):
    return ucType & 0x1F


class LexemeType(object):
    conv = {
        0: "неопр.",
        1: "глаг.",
        2: "сущ.",
        3: "прил.",
        4: "местим.",
        5: "числ.",
        6: "им.собств.",
        7: "вводн.",
        8: "межд.",
        9: "предикатив",
        10: "предл.",
        11: "союз",
        12: "част.",
        13: "нар.",
        14: "сравн.",
        15: "аббрев.",
        16: "числ.",
        17: "прич.",
        18: "кр.прич.",
        19: "дееприч.",
        20: "кратк.прил.",
        21: "артик.",
    }

    rev_conv = dict((reversed(item) for item in list(conv.items())))

    def __init__(self, val):
        if isinstance(val, str):
            self.num = self.rev_conv[val]
        else:
            self.num = val

    def __str__(self):
        return self.conv[self.num]

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.num == other.num


class MorphCase:
    conv = {
        0x00: "неопр.",
        0x01: "им.",
        0x02: "род.",
        0x04: "дат.",
        0x08: "вин.",
        0x10: "твор.",
        0x20: "предл.",
        0x40: "род.2",
        0x80: "предл.2",
    }

    rev_conv = dict((reversed(item) for item in list(conv.items())))

    def __init__(self, val):
        if isinstance(val, str):
            self.num = self.rev_conv[val]
        else:
            self.num = val

    def __str__(self):
        return self.conv[self.num]

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.num == other.num


class MorphGender:
    conv = {0x000: "неопр.", 0x100: "мужск.", 0x200: "женск.", 0x400: "сред."}

    rev_conv = dict((reversed(item) for item in list(conv.items())))

    def __init__(self, val):
        if isinstance(val, str):
            self.num = self.rev_conv[val]
        else:
            self.num = val

    def __str__(self):
        return self.conv[self.num]

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.num == other.num


class MorphNumber:
    conv = {0x000: "неопр.", 0x0800: "ед.", 0x1000: "множ."}

    rev_conv = dict((reversed(item) for item in list(conv.items())))

    def __init__(self, val):
        if isinstance(val, str):
            self.num = self.rev_conv[val]
        else:
            self.num = val

    def __str__(self):
        return self.conv[self.num]

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.num == other.num


class MorphTense:
    conv = {
        0x0000: "неопр.",
        0x2000: "инф.",
        0x4000: "безл.",
        0x6000: "буд.",
        0x8000: "наст.",
        0xA000: "прош.",
    }

    rev_conv = dict((reversed(item) for item in list(conv.items())))

    def __init__(self, val):
        if isinstance(val, str):
            self.num = self.rev_conv[val]
        else:
            self.num = val

    def __str__(self):
        return self.conv[self.num]

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.num == other.num


class MorphPerson:
    conv = {0x0000: "неопр.", 0x10000: "1л.", 0x20000: "2л.", 0x30000: "3л."}

    rev_conv = dict((reversed(item) for item in list(conv.items())))

    def __init__(self, val):
        if isinstance(val, str):
            self.num = self.rev_conv[val]
        else:
            self.num = val

    def __str__(self):
        return self.conv[self.num]

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.num == other.num


class Lexeme:
    def __init__(self, node):
        self.node = node

    def get_lemma(self):
        return self.node["sDictForm"]

    def get_lexeme_type(self):
        return LexemeType(get_lexeme_type(self.node["ucType"]))

    def get_cases(self):
        return [MorphCase(item) for item in get_cases(self.node["dwInfo"])]

    def get_genders(self):
        return [MorphGender(item) for item in get_genders(self.node["dwInfo"])]

    def get_numbers(self):
        return [MorphNumber(item) for item in get_numbers(self.node["dwInfo"])]

    def get_tense(self):
        # return [MorphTense(item) for item in get_tenses(self.node["dwInfo"])]
        return MorphTense(get_tense(self.node["dwInfo"]))

    def get_whole_morph(self):
        return self.node["dwInfo"]

    def get_persons(self):
        return [MorphPerson(item) for item in get_persons(self.node["dwInfo"])]

    def __repr__(self):
        return "<lemma={} type={}{}{}{}{}{}>".format(
            self.get_lemma().encode("utf8"),
            str(self.get_lexeme_type()),
            make_str_from_l(self.get_cases(), " cases="),
            make_str_from_l(self.get_genders(), " genders="),
            make_str_from_l(self.get_numbers(), " numbers="),
            make_str_from_v(self.get_tense(), " tense="),
            make_str_from_l(self.get_persons(), " persons="),
        )

    def __str__(self):
        return self.__repr__()


class Word:
    def __init__(self, node):
        self.length = node["iLength"]
        self.form = node["sWord"]
        self.offset = node["iOffset"]
        self.lexemes = list()

        for lexeme in node["base"]:
            self.lexemes.append(Lexeme(lexeme))

    def get_lexeme(self, num=0):
        return self.lexemes[num]

    def get_form(self):
        return self.form

    def get_offset(self):
        return self.offset

    def get_length(self):
        return self.length

    def __str__(self):
        return "<offset={} length={} form={}>".format(
            self.offset, self.length, self.form
        )
